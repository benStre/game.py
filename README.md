## GamePY - a simple python game engine based on pygame

### Run GamePY

To run *GamePy*, you need to download [game.py](https://gitlab.com/benStre/game.py/blob/master/game.py).
Run the example game *basic.py* :

 **Windows:** 
 
```bash
game.py basic
```
 
  **Linux**
```bash
./game.py basic
```

 **... or directly with python** :
 
```bash
python game.py basic
```

*GamePy* is working both with Python2 and Python3
<br>

### Basic setup

```python
# INIT function - called once when starting
def init():
	SETTINGS.title = "Hello World"  # game title (window title)


# UPDATE function - is continuously called every frame
def update():
	# draw red circle with radius 10 at current mouse position 
	CANVAS.drawCircle(CANVAS.mouseX(), CANVAS.mouseY(), 10, RED)
	
```

This basic game setup works like a simple drawing program

### Settings

There are several (optional) settings that can be defined in the init-function :

```python
#INIT function - called once when starting
def init():
	SETTINGS.windowSize = (900,600)  # window dimensions (default: 640x480)
	SETTINGS.fullscreen = False # fullscreen window (default: False)
	SETTINGS.fps = 600 # desired frame rate (default: 500)
	SETTINGS.showDebugInfo = True # show frame rate and other information (default: False)
	SETTINGS.mouseVisible = False # mouse visibility (default: True)
	SETTINGS.title = "Hello World"  # the name of your game
	SETTINGS.icon = "red.png" # game icon path or url
	
```

------
### Other included modules

There are four other modules that are automatically initialised when the game starts 

#### Connection

The *CONNECTION* module is a simple way to connect multiple players in the same LAN to create multiplayer games.
There a two types of connection endpoints that communicate over socket connections:

 - **Servers**  
 - **Clients** 

Normally, there are multiple clients that connect to one server. The players have to decide who runs a server and then are able to easily find it and connect.

<br>

To start a server, you just need to call 
```python
 CONNECTION.server()
```

If a client sends a message to the server, the function *receivedMessage*, which you can just insert into your python file, will be called:

```python
def receivedMessage(client, message, response):
	print("client ip: " + client + ", message: " + message) # output client ip and message
	response.send("okay") # send response message to client
```
<br>

If you want to setup a client, you first need to call 
```python
CONNECTION.getServers()
```

After one or more servers were found, the *serversAvailable* callback function will be called and you get a list of all available servers, which you can now connect to:

```python
def serversAvailable(servers):
	print(servers) # output the server list 
				   # -> [{'ip': '192.168.0.25', 'name': 'player1'}, {'ip': '192.168.0.21', 'name': 'player2'}]
	CONNECTION.client(servers[0]) # create a client and connect to the first server of the list
```

To set your name (as a server or client), you can set the variable *playerName* anywhere in your python program:

    playerName = "player1"


#### Data


There are two functions for accessing your permanently available variables that will be saved in a *.db* after the game is closed :
```python
DATA.set("var1", 10) # set var1 = 10 (strings are also possible)
print(DATA.get("var1")) # get value of var1 and print it
```

#### Sound
To play sound in your game, there are two options:

Play an mp3, wav, or ogg audio file:
```python
SOUND.play("audio.mp3")
```
	
Play a certain frequency for a certain time:
```python
SOUND.frequency(440.0, 1000) # frequency 440.0 Hz, duration 1000ms
```


#### Canvas

The canvas is the most important game module, as it provides the drawing functions:
```python
CANVAS.drawLine(x1, y1, x2, y2, color) # draw a line from (x1, y1) to (x2, y2)
CANVAS.drawAALine(x1, y1, x2, y2, color) # draw a line with anti-aliasing

CANVAS.drawCircle(x, y, radius, color, strokeWidth) # draw a circle at (x,y), strokeWidth 0 means filled
CANVAS.drawAACircle(x, y, radius, color, strokeWidth) # draw a circle with anti-aliasing

CANVAS.drawRect(x, y, width, height, color, strokeWidth) # draw a rect at (x,y)

CANVAS.drawImage(img, x, y) # draw a preloaded image at (x,y)
CANVAS.drawPixel(x, y, color) # draw a pixel at (x,y)

CANVAS.drawText(x, y, text, color) # render colored text at (x,y)

CANVAS.fillScreen(color) # fill the whole screen with a color

```
<br>

To preload the later needed images, there are special functions that should be called in the *init* function:

```python
img1 = CANVAS.loadImage("img1.png", (width,height)) # load image from local file system + set width and height
img2 = CANVAS.loadImage("https://example.com/img2.png", (width,height)) # load image from url

imgTile = CANVAS.loadImageTile("img_map.png", (mapX, mapY, mapWidth, mapHeight), (width,height)) # load tile image from an image map, from the rectangle (mapX, mapY, mapWidth, mapHeight) and set width and height
```

-----

### Constants

#### Colors

Colors must be defined as a tuple or list in the rgb-format 

```python
myColor = (red, green, blue) # rgb values from 0-255
```
There are some predefined colors:

![#f03c15](https://placehold.it/15/ffffff/000000?text=+) `WHITE`<br>
![#f03c15](https://placehold.it/15/000000/000000?text=+) `BLACK`<br>
![#f03c15](https://placehold.it/15/646464/000000?text=+) `GREY`<br>

![#f03c15](https://placehold.it/15/c83232/000000?text=+) `RED`<br>
![#f03c15](https://placehold.it/15/3232c8/000000?text=+) `BLUE`<br>
![#f03c15](https://placehold.it/15/0d8200/000000?text=+) `GREEN`<br>

![#f03c15](https://placehold.it/15/ffcc00/000000?text=+) `YELLOW`<br>
![#f03c15](https://placehold.it/15/dd00ff/000000?text=+) `MAGENTA`<br>
![#f03c15](https://placehold.it/15/00dcff/000000?text=+) `CYAN`<br>



### Event functions

Besides the *init* and *update* function, there are serveral event function that are automatically called, when an event happens:

#### Mouse

```python
def mousedown(pos):
	x = pos[0]
	y = pos[1]
	print("mouse down at " + str(x) + "|" + str(y))
    
def mouseup(pos):
	x = pos[0]
	y = pos[1]
	print("mouse up at " + str(x) + "|" + str(y))
	    
def mousemove(pos):
	x = pos[0]
	y = pos[1]
	print("mouse moved to " + str(x) + "|" + str(y))
```

#### Keys

```python
def keydown(key):
	print("keycode: " + str(key.code) + ", char: " + key.char)
    # -> output "keycode: 97, char: a"
```



