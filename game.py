#!/usr/bin/python2
# -*- coding: utf-8 -*-
from __future__ import division
from subprocess import call
import time, random, math, sys, os, importlib, socket, io
from importlib import import_module
from threading import Thread
import traceback

import platform

import warnings

if not sys.warnoptions:
    warnings.simplefilter("ignore")

try:
    # Python2
    from urllib2 import urlopen
except ImportError:
    # Python3
    from urllib.request import urlopen
	
try:
    # Python2
	from SocketServer import ThreadingMixIn
except ImportError:
    # Python3
	from socketserver import ThreadingMixIn


WHITE = (255,255,255)
RED = (200, 50, 50)
BLUE = (50, 50, 200)
BLACK = (0, 0, 0)
YELLOW = (255, 204, 0)
GREEN = (13, 130, 0)
MAGENTA = (221, 0, 255)
CYAN = (0, 220, 255)
GREY = (100, 100, 100)


def connection(i):
	return i[2]
	
class sound:
	def __init__(self, i):
		self.pygame = i[0]
		
	def frequency(self, f, d):
		
		soundthread = SoundThread(f, d)
		soundthread.start()
		
	def play(self, file):
		self.pygame.mixer.music.load(file)
		self.pygame.mixer.music.play(0)
	

class data:
	def __init__(self, i):
		
		import sqlite3
		
		self.dbname = i[4] + ".db"
		self.db = sqlite3.connect(self.dbname)
		self.cursor = self.db.cursor()
		self.cursor.execute('CREATE TABLE IF NOT EXISTS data(key TEXT, value TEXT)')
		self.db.commit()
		
	def set(self, key, value):
		
		self.cursor.execute('INSERT OR IGNORE INTO data (key,value) VALUES(:key, :value);', 
				{'key':unicode(str(key), "utf-8"), 'value':unicode(str(value), "utf-8")})
		self.cursor.execute('UPDATE data SET value = :value WHERE key = :key', 
				{'key':unicode(str(key), "utf-8"), 'value':unicode(str(value), "utf-8")})
		self.db.commit()
		
	def get(self, key):
	
		self.cursor.execute('SELECT value from data WHERE key = :key;', 
				{'key':key})
		self.db.commit()
		
		res = self.cursor.fetchall()[0][0]
		try:
			return int(res)
		except:
			return res

class canvas:


	def __init__(self, i):
		self.screen = i[1]
		self.pygame = i[0]

	def fillScreen(self, color):
		self.screen.fill(color)
	def drawRect(self, x, y, w, h, color, s = 0):
		self.pygame.draw.rect(self.screen, color, (x, y, w, h), s)
	def drawCircle(self, x, y, r, color, s = 0):
		self.pygame.draw.circle(self.screen, color, (x, y), r, s)
	def drawAACircle(self, x, y, r, color, s = 0):
		if s == 0:
			self.pygame.gfxdraw.filled_circle(self.screen, x, y, r, color)
		else:	
			self.pygame.gfxdraw.aacircle(self.screen, x, y, r, color)
		
	def drawImage(self, t, x, y):
		self.screen.blit(t,(x, y))
	def drawLine(self, x1, y1, x2, y2, color):
		self.pygame.draw.line(self.screen, color, (x1, y1), (x2, y2))
	def drawAALine(self, x1, y1, x2, y2, color):
		self.pygame.draw.aaline(self.screen, color, (x1, y1), (x2, y2))
	def drawPixel(self, x, y, color):
		self.pygame.gfxdraw.pixel(self.screen, x, y, color)
	def drawText(self, x, y, text, color):
		textsurface = font.render(text, False, color)
		self.screen.blit(textsurface,(x,y))		
		
	def loadImageTile(self, src, cropRect, newSize):
		image = self.getImage(src)
		crop_rect = cropRect#(i*16, j*16, 16, 16)
		return self.pygame.transform.scale(image.subsurface(crop_rect), newSize)
	def loadImage(self, src, newSize):
		image = self.getImage(src)
		return self.pygame.transform.scale(image, newSize)

	def getImage(self, name):
		if name.startswith("http://") or name.startswith("https://"):
			image_url = name
			image_str = urlopen(image_url).read()
			image_file = io.BytesIO(image_str)
			return self.pygame.image.load(image_file).convert_alpha()
		else :
			return self.pygame.image.load(name).convert_alpha()
			
			
	def mouseX(self):
		return self.pygame.mouse.get_pos()[0]
		
	def mouseY(self):
		return self.pygame.mouse.get_pos()[1]
		
		
def install_module(name):
	if "Windows" in platform.system():
		call(["python", "-m", "pip", "install", name])
	elif "Linux"  in platform.system() or "Ubuntu"  in platform.system() or  "Mac" in platform.system():
		call(["pip", "install", name])


def print_info(p):
	print(colorama.Fore.GREEN + '> ' + str(p) + colorama.Style.RESET_ALL)

def print_error(p):
	print(colorama.Fore.RED + '> [ERROR] ' + str(p) + colorama.Style.RESET_ALL)

def print_exception():
	exc_type, exc_value, exc_traceback = sys.exc_info()
	info = traceback.format_exception(exc_type, exc_value, exc_traceback)
	print_error("\n" + info[2] + "    " + info[-1].replace("\n", ""))
	
def print_blue(p):
	print(colorama.Fore.CYAN + '> ' + str(p) + colorama.Style.RESET_ALL)


def get_ip():
	try:
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		s.connect(("8.8.8.8", 80))
		ip = s.getsockname()[0]
		s.close()
		return ip
	except: return colorama.Fore.RED + '[ERROR] no network available' + colorama.Style.RESET_ALL

def drawDebugInfo(info):
	textsurface = font.render(info, False, WHITE, GREY)
	screen.blit(textsurface,(0,0))


current_milli_time = lambda: int(round(time.time() * 1000))


class SocketManager:


	def client(self, server):
		self.type="client"
		self.server_ip = server.ip

		self.socket_threads()
		


	def getServers(self):
	
		getserversthread = GetServersThread()
		getserversthread.start()
		
	def server(self):
		self.type="server"
		self.socket_threads()


	def socket_threads(self):

		if self.type == "server":
			
			TCP_IP = get_ip()

			print_info("SERVER start as " + TCP_IP)

			TCP_PORT = 12343

			self.tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			self.tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
			self.tcpsock.bind((TCP_IP, TCP_PORT))

			waitingthread = WaitingThread(self.tcpsock)
			waitingthread.start()

		else:
			print_info("CLIENT connect with server " +  self.server_ip)
			TCP_IP = self.server_ip
			TCP_PORT = 12343

			self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			self.s.connect((TCP_IP, TCP_PORT))
			
			time.sleep(0.3)
			self.send("MYNAME"+thegame.SETTINGS.playerName.encode())
			
	def send(self, message):
		if self.type == "client":
			#print_info("CLIENT send " + message)
			m = message + "_END"
			self.s.send(m.encode())

			#data = self.s.recv(1024)
			#print("server said: "+data)
			
class GetServersThread(Thread):

	def is_open(self, ip):
		try :
			s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			s.settimeout(0.2)
			result = s.connect_ex((ip,12343))
			if result == 0:
			   return 1
			else:
			   return 0
		except:
			return 0
			
	def get_name(self, ip):
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect((ip, 12343))
		m = "GETNAME_END"
		s.send(m.encode())
		data = s.recv(1024)
		return data
		
	def __init__(self):
		Thread.__init__(self)
		
	def run(self):
		
		own_ip = get_ip().rsplit('.', 1)[0] + "."
		
		print_info("FIND SERVERS " + own_ip + "*")

		#print("own ip:" + own_ip);
		port = 12343

		servers = []
		serverList = []
		
		for n in range(1,255): 
			new_ip = own_ip+str(n)
			#print("..." + new_ip)

			if(self.is_open(own_ip+str(n))):
				#print("open :" + new_ip)
				name = self.get_name(new_ip);
				servers.append(remotePlayer(new_ip, name))
				serverList.append(vars(servers[-1]))
				print_info("FOUND SERVERS " + str(serverList))
				thegame.serversAvailable(servers)
				
class ClientThread(Thread):

	def __init__(self,ip,port):
		Thread.__init__(self)
		self.info = remotePlayer(ip, "_name_")


	def run(self):
		global conn
		while True:
			data = conn.recv(2048).decode()
			if not data or data == "_END" : break
			if data[:-4] == "GETNAME":
				conn.send(thegame.SETTINGS.playerName.encode())
			elif data[0:6] == "MYNAME":
				self.info.name = data[6:-4]
				print_info("SERVER client "+self.info.name+ "/" + self.info.ip +" connected")

			else:
				try : thegame.receivedMessage(self.info, data[:-4], conn)
				except : pass

				
class SoundThread(Thread):
	def __init__(self, f, d):
		Thread.__init__(self)

		self.f = f
		self.d = d

	def run(self):
		from pydub import AudioSegment
		from pydub.generators import Sine
		from pydub.playback import play
		tone = Sine(self.f).to_audio_segment(duration=self.d)
		# Play tone
		play(tone)
		
		
class WaitingThread(Thread):

	def __init__(self, tcpsock):
		Thread.__init__(self)

		self.tcpsock = tcpsock

	def run(self):
		global conn
		while True:
			threads = []

			while True:
				self.tcpsock.listen(4)
				(conn, (ip,port)) = self.tcpsock.accept()
				newthread = ClientThread(ip,port)
				newthread.start()
				threads.append(newthread)
				break

			for t in threads:
				t.join()

				
def quit_game():
	print_blue("QUIT")
	pygame.quit()
	os._exit(1)

def restart_game():
	print_blue("RESTART")
	reload(thegame)
	init()
	
class key:
	def __init__(self, code, char):
		self.code = code
		self.char = char
		
class remotePlayer:
	def __init__(self, ip, name):
		self.ip = ip
		self.name = name
		
def gameloop():

	global font, screen, pygame, thegame
	

	updateInfo = 21
	last_millis = 0

	while True:

		# Events
		event = pygame.event.poll()
		if event.type == QUIT:
			quit_game()
		elif event.type == KEYDOWN:
			try: thegame.keydown(key(event.key, event.unicode))
			except AttributeError: pass
			except Exception as e: print_exception()
		elif event.type == pygame.MOUSEBUTTONDOWN:
			try: thegame.mousedown(pygame.mouse.get_pos())
			except AttributeError: pass
			except Exception as e: print_exception()
		elif event.type == pygame.MOUSEBUTTONUP:
			try: thegame.mouseup(pygame.mouse.get_pos())
			except AttributeError: pass
			except Exception as e: print_exception()
		elif event.type == pygame.MOUSEMOTION:
			try: thegame.mousemove(pygame.mouse.get_pos())
			except AttributeError: pass
			except Exception as e: print_exception()

		try: thegame.update()
		except Exception as e: print_exception(); quit_game()
		time.sleep(1/thegame.SETTINGS.fps)


		updateInfo+=1
		millis = current_milli_time()
		try: 
			FPS = int(round(1000/(millis-last_millis)))
			last_millis = millis
			if(updateInfo>20):
				info = str(FPS)+" fps"
				updateInfo=0
		except: pass

		if thegame.SETTINGS.showDebugInfo:
			drawDebugInfo(info)

		pygame.display.update()

		
class settings:
	pass

def init():

	global font, screen, pygame, thegame

	
	bd = 32
	
	pygame.init()

	screen = pygame.display.set_mode((1, 1), DOUBLEBUF , bd)

	socket = SocketManager()
	
	i = (pygame, screen, socket, pydub, sys.argv[1].strip('.py'))
	
	thegame.CONNECTION = connection(i) # "connection" -> connect to other players
	thegame.CANVAS = canvas(i) # "canvas" -> draw on screen
	thegame.SOUND = sound(i) # "sound" -> play frequencies and audio files
	thegame.DATA = data(i) # "data" -> save game data permanently
	
	thegame.WHITE = WHITE
	thegame.RED = RED
	thegame.BLUE = BLUE
	thegame.BLACK = BLACK
	thegame.YELLOW = YELLOW
	thegame.GREEN = GREEN
	thegame.MAGENTA = MAGENTA
	thegame.CYAN = CYAN
	thegame.GREY = GREY
	
	thegame.SETTINGS = settings()
	
	thegame.SETTINGS.playerName = "new_player"
	thegame.SETTINGS.showDebugInfo = False
	thegame.SETTINGS.title = "Game"
	thegame.SETTINGS.mouseVisible = True
	thegame.SETTINGS.fps = 500
	thegame.SETTINGS.fullscreen = False
	thegame.SETTINGS.windowSize = (640, 480)

	thegame.QUIT = quit_game
	thegame.RESTART = restart_game

	thegame.init()
	
	pygame.font.init()
	font = pygame.font.SysFont('Comic Sans MS', 13)



	### settings
	
	if thegame.SETTINGS.fullscreen:
		flags = DOUBLEBUF | FULLSCREEN
		screen = pygame.display.set_mode((thegame.SETTINGS.windowSize[0], thegame.SETTINGS.windowSize[1]), flags , bd)
	else :
		flags = DOUBLEBUF
		screen = pygame.display.set_mode((thegame.SETTINGS.windowSize[0], thegame.SETTINGS.windowSize[1]), flags , bd)
	
	screen.fill(WHITE)
	pygame.display.update()

	
	pygame.display.set_caption(thegame.SETTINGS.title)

	if hasattr(thegame.SETTINGS, 'icon'):
		icon = canvas((pygame, screen)).getImage(thegame.SETTINGS.icon)
		icon.set_colorkey((255, 255, 255))
		pygame.display.set_icon(pygame.transform.scale(icon, (32,32)))
	
	pygame.mouse.set_visible(thegame.SETTINGS.mouseVisible)

	print("")
	print_info("launching \"" + thegame.SETTINGS.title+ "\"")



	gameloop()


#####import other moduls and init################################
if __name__ == "__main__":

	try :
		import colorama
	except:
		print("> INSTALLING colorama")
		install_module("colorama")
		import colorama
	colorama.init()
	

	
	try :
		import pygame
	except:
		print_info("INSTALLING pygame")
		install_module("pygame")
		import pygame
	from pygame.locals import *
	from pygame import gfxdraw
	
	
	

	try :
		import pydub
	except:
		print_info("INSTALLING pydub")
		install_module("pydub")
		import pydub
	from pydub import AudioSegment
	from pydub.generators import Sine
	from pydub.playback import play
	
	
	try :
		import sqlite3
	except:
		print("> INSTALLING sqlite3")
		install_module("sqlite3")
		import sqlite3
	

	print_blue("OS : " + platform.system())
	print_blue("IP : " + get_ip())

	update_string = sys.argv[1].strip('.py')
	thegame = import_module(update_string)

	init()

#########################################################
