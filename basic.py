# INIT function - called once when starting
def init():
	SETTINGS.title = "Hello World"  # game title (window title)


# UPDATE function - is continuously called every frame
def update():
	# draw red circle with radius 10 at current mouse position 
	CANVAS.drawCircle(CANVAS.mouseX(), CANVAS.mouseY(), 10, RED)
